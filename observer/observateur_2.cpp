#include <iostream>
#include <string>
#include <vector>


// On implémente cette fois le pattern avec un transfert de donnée entre le sujet et les observateurs grâce à une référence sur le sujet dans chaque observateur et un getter dédié. 

class Subject; 

class Observer 
{
public: 
    virtual void update() = 0;
};

class Subject 
{
public:    
    virtual void addObserver(Observer* observer) 
    {
        m_observers.push_back(observer); 
    }
    virtual void removeObserver(Observer* observer) 
    {
        m_observers.erase(std::remove(m_observers.begin(), m_observers.end(), observer), m_observers.end());
    }
    virtual void notify() = 0;
    virtual std::string getData() = 0; 
    
protected: 
    std::vector<Observer*> m_observers;
};


class Shuttle : public Subject 
{
public:
Shuttle() = default;

void setData(const std::string& data) { m_data = data ;}
std::string getData() {return m_data;}

void notify() 
{
    for (Observer* observer : m_observers) { observer->update(); }
}

private:
std::string m_data; 
};

class Lapin : public Observer 
{
public:     

    Lapin(Subject* subject) : m_subject(subject) {}

    void update() 
    {
    std::cout << "Lapin voit " + m_subject->getData() << std::endl;
    }
    
private: 
    Subject* m_subject; 
};

class NavLead : public Observer
{

public: 

    NavLead(Subject* subject) : m_subject(subject) {} 
    
    void update() 
    {
    std::cout << "NavLead voit " + m_subject->getData() << std::endl;
    }
    
private: 
    Subject* m_subject; 

};

int main()
{    
    
    Shuttle myShuttle;
    
    Lapin lapin(&myShuttle);
    NavLead navlead(&myShuttle);
    
    myShuttle.addObserver(&lapin);
    myShuttle.addObserver(&navlead);
    
    myShuttle.setData("Nav-sim-101");
    myShuttle.notify(); 
    
    myShuttle.removeObserver(&navlead);
    
    myShuttle.setData("Nav-sim-102");
    myShuttle.notify(); 
    
}