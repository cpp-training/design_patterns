#include <iostream>
#include <string>
#include <vector>


// Première implémentation simple du pattern observateur. Le sujet transmet les données d'intérêt directement dans la signature de la méthode 'update'. Ce n'est pas idéal si ce format de 
// données est amené à changer. 

class Subject; 

class Observer 
{
public: 
    virtual void update(const std::string& data) = 0;
};

class Subject 
{
public:    
    virtual void addObserver(Observer* observer) 
    {
        m_observers.push_back(observer); 
    }
    virtual void removeObserver(Observer* observer) 
    {
        m_observers.erase(std::remove(m_observers.begin(), m_observers.end(), observer), m_observers.end());
    }
    virtual void notify() = 0;
    
protected: 
    std::vector<Observer*> m_observers;
};


class Shuttle : public Subject 
{
public:
Shuttle() = default;
void setData(const std::string& data) { m_data = data ;}

void notify() 
{
    for (Observer* observer : m_observers) { observer->update(m_data); }
}

private:
std::string m_data; 
};

class Lapin : public Observer 
{
    void update(const std::string& data) 
    {
    std::cout << "Lapin voit " + data << std::endl;
    }
};

class NavLead : public Observer
{
    void update(const std::string& data) 
    {
    std::cout << "NavLead voit " + data << std::endl;
    }

};

int main()
{    
    Lapin lapin;
    NavLead navlead;
    
    Shuttle myShuttle;
    
    myShuttle.addObserver(&lapin);
    myShuttle.addObserver(&navlead);
    
    myShuttle.setData("Nav-sim-101");
    myShuttle.notify(); 
    
    
}