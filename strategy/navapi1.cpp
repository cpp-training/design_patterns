#include <iostream>
#include <string>

// On modélise une simple API entre des utilisateurs de Navettes et les navettes. Ces utilisateurs peuvent envoyer des requêtes de mission avec différents formats (F1, F2, F3 ...).
// On écrit ici un code client fortement dépendant du traitement donné aux requêtes de missions.

enum class MissionType {
    F1,
    F2,
    F3
};

class MissionRequest {

public:
    MissionRequest(const MissionType& mission_type, const std::string& data) : m_type(mission_type), m_data(data) {}
    
    MissionType getMissionType() const {return m_type;}
private:
    MissionType m_type;
    std::string m_data; 
};


class NavAPI {

    public:
    MissionRequest receiveRequest(); 
    
    void extractAndSendMission(const MissionRequest& mission_request) 
    {
        MissionType mission_type = mission_request.getMissionType();
        switch (mission_type)
        {
            case MissionType::F1:
                handleF1mission();
                break;
            case MissionType::F2:
                handleF2mission();
                break;
            case MissionType::F3:
                handleF3mission();
                break;
        }
    
    
        // Do some other stuff
    }
    
    private:
    void handleF1mission() {std::cout << "F1 mission here" << std::endl;}
    void handleF2mission() {std::cout << "F2 mission here" << std::endl;}
    void handleF3mission() {std::cout << "F3 mission here" << std::endl;}

};


int main()
{   
    NavAPI navAPI; 
    
    // a client send a request
    MissionRequest mission_request(MissionType::F1, "Mission de type F1");
    
    navAPI.extractAndSendMission(mission_request); 
}