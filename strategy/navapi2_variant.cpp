#include <iostream>
#include <cstdlib>
#include <string>
#include <memory>

// On effectue ici une première amélioration (encapsuler ce qui varie) en déléguant par composition le traitement des missions à un objet. Le client (NavAPI) est maintenant découplé du
// traitement donné aux missions.

enum class MissionType 
{
    F1,
    F2,
    F3
};

class MissionRequest 
{

public:
    MissionRequest(const MissionType& mission_type, const std::string& data) : m_type(mission_type), m_data(data) {}
    
    MissionType getMissionType() const {return m_type;}
private:
    MissionType m_type;
    std::string m_data; 
};

class MissionExtractor 
{
    public: 
    void extractMission(const MissionRequest& mission_request) {
        
        MissionType mission_type = mission_request.getMissionType();
        switch (mission_type)
        {
            case MissionType::F1:
                handleF1mission(mission_request);
                break;
            case MissionType::F2:
                handleF2mission(mission_request);
                break;
            case MissionType::F3:
                handleF3mission(mission_request);
                break;
        }
    }
    
    private:
    void handleF1mission(const MissionRequest& mission_request){std::cout << "F1 mission here" << std::endl;}
    void handleF2mission(const MissionRequest& mission_request){std::cout << "F2 mission here" << std::endl;}
    void handleF3mission(const MissionRequest& mission_request){std::cout << "F3 mission here" << std::endl;}
}; 


class NavAPI {

    public:
    MissionRequest receiveRequest(); 
    
    void extractAndSendMission(const MissionRequest& mission_request) 
    {
        m_mission_extractor.extractMission(mission_request); 
    
        // Do some other stuff
    }
    
    private:
    MissionExtractor m_mission_extractor; 

};


int main()
{   
    NavAPI navAPI; 
    
    // a client send a request
    MissionRequest mission_request(MissionType::F2, "Mission de type F2");
    
    navAPI.extractAndSendMission(mission_request); 
}