#include <iostream>
#include <string>

// On modélise un jeu avec deux canards, un mandarin et un colvert. La demande client risque d'évoluer. 

class Canard {
public: 
    virtual void cancaner() = 0; 
    virtual void nager() = 0;
    virtual void afficher() = 0;

};

class Colvert : public Canard 
{
public: 
    void cancaner() {std::cout << " Coincoin" << std::endl;} 
    void nager() {}
    void afficher() {std::cout << "Je suis un Colvert" << std::endl;}

};

class Mandarin : public Canard 
{
public: 
    void cancaner() {std::cout << " Coincoin" << std::endl;} 
    void nager() {}
    void afficher() {std::cout << "Je suis un Mandarin" << std::endl;}
}; 


int main()
{    
    Mandarin mandarin; 
    Colvert colvert; 
    
    mandarin.afficher();
    colvert.afficher();
}
