#include <iostream>
#include <string>

// On utilise cette fois le pattern stratégie pour anticiper les nouveaux formats de mission qui pourraient apparaitre. On lie NavAPI à une interface (MissionExtractor) et non une implémentation.

enum class MissionType 
{
    F1,
    F2,
    F3
};

class MissionRequest 
{

public:
    MissionRequest(const MissionType& mission_type, const std::string& data) : m_type(mission_type), m_data(data) {}
    
    MissionType getMissionType() const {return m_type;}
private:
    MissionType m_type;
    std::string m_data; 
};

class MissionExtractor 
{
    public: 
    virtual void extractMission(const MissionRequest& mission_request) = 0;
}; 

class F3Extractor : public MissionExtractor 
{
public:
    void extractMission(const MissionRequest& mission_request) { std::cout << "F3 mission here" << std::endl; }
};

class F2Extractor : public MissionExtractor 
{
public:
    void extractMission(const MissionRequest& mission_request) { std::cout << "F2 mission here" << std::endl; }
};

class F1Extractor : public MissionExtractor 
{
public:
    void extractMission(const MissionRequest& mission_request) { std::cout << "F1 mission here" << std::endl; }
};



class NavAPI {

    public:
    NavAPI(MissionExtractor* mission_extractor) : m_mission_extractor(mission_extractor) {}
    MissionRequest receiveRequest(); 
    
    void extractAndSendMission(const MissionRequest& mission_request) 
    {
        m_mission_extractor->extractMission(mission_request); 
    
        // Do some other stuff
    }
    
    private:
    MissionExtractor* m_mission_extractor; 

};


int main()
{   
    F1Extractor f1_extractor;
    NavAPI navAPI(&f1_extractor); 
    
    // a client send a request
    MissionRequest mission_request(MissionType::F1, "Mission de type F1");
    
    navAPI.extractAndSendMission(mission_request); 
}