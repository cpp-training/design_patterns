#include <iostream>
#include <string>

// On veut maintenant que les canards volent, mais pas tous (on veut pouvoir choisir), et on introduit un canard en plastique. Quelle conception adopter? Utilisation du pattern stratégie!  

// Classe abstraite Stratégie
class ComportementVol 
{
public: 
    virtual void voler() =0; 
};


// Nos instances de stratégies 
class VolerAvecAiles : public ComportementVol 
{
public:
    void voler() {std::cout << "Je vole avec des ailes!" << std::endl; }
};

class NePasVoler : public ComportementVol 
{
public:
    void voler() {std::cout << "Je ne vole pas" << std::endl; }
};


// Classe abstraite du canard
class Canard 
{
public: 
    
    Canard(ComportementVol* comportement_vol) : m_comportement_vol(comportement_vol) {}
    
    virtual void cancaner() = 0; 
    virtual void nager() = 0;
    virtual void afficher() = 0;
    virtual void voler() { m_comportement_vol->voler();}

protected:
    ComportementVol* m_comportement_vol; 
};


// Instances de canards 
class Colvert : public Canard 
{
public: 

    Colvert(ComportementVol* comportement_vol) : Canard(comportement_vol) {}
    
    void cancaner() {std::cout << " Coincoin" << std::endl;} 
    void nager() {}
    void afficher() {std::cout << "Je suis un Colvert" << std::endl;}

};

class Mandarin : public Canard 
{
public: 
    Mandarin(ComportementVol* comportement_vol) : Canard(comportement_vol) {}
    void cancaner() {std::cout << " Coincoin" << std::endl;} 
    void nager() {}
    void afficher() {std::cout << "Je suis un Mandarin" << std::endl;}
}; 

class CanardPlastique : public Canard 
{
public: 
    CanardPlastique(ComportementVol* comportement_vol) : Canard(comportement_vol) {}
    void cancaner() {std::cout << " Couine couine " << std::endl;} 
    void nager() {}
    void afficher() {std::cout << "Je suis un Canard en plastique" << std::endl;}
}; 



int main()
{    
    VolerAvecAiles vol_ailes;
    NePasVoler ne_pas_voler;
    
    Mandarin mandarin(&vol_ailes); 
    Colvert colvert(&vol_ailes); 
    CanardPlastique canPlastique(&ne_pas_voler); 
    
    mandarin.afficher();
    colvert.afficher();
    canPlastique.cancaner(); 
    
    mandarin.voler();
    canPlastique.voler();
}